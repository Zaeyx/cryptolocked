"""
cryptolocked version 1.0 6/18/2014

Command line arguments
for debug mode (functionality check): debug
to arm from the command line: armed
for tentacles (see below): tentacles
"""

import random
import os
import sys
import time
import hashlib
import signal
import smtplib

if len(sys.argv) == 1:
	sys.argv.append("NULL")
	
#Creates the random data to fill a file with
def rand_data():
	dat = ''
	for i in range(100):
		dat = dat + str(random.randint(1,10000))
	return dat

#To configure email alerts, modify these two addresses
#The from address must be a gmail account
fromaddr = "user@gmail.com"
#you must supply the username and password for this email address
username = 'username'
password = 'password'
#Next set the to email
#This can be the same as the from email
toaddr = "user@domain.com"
#Finally, set enabled to true
alerts_enabled=False
#Set sensitive alerts to False to disable enhanced reporting (processes and host information)
#If you're working with confidential systems you may not want this information stored in an email
sensitive_alerts=True

#If tentacles is set to false cryptolocked will only use one simply named file
#If tentacles is set to true cryptolocked will use an assortment of non-standard files
#This is far messier, and easier to trip.
#But it is also more likely to capture activity before much damage can be done
#And it makes it far harder for an advanced adversary to avoid the tripfiles
tentacles = False

#Enable if first argument is tentacles
if sys.argv[1] == "tentacles":
	tentacles = True
#No tentacles in debug mode
if sys.argv[1] == "debug":
	tentacles = False

#Only worry about these if tentacles is set to False
filename = "test.txt"
content = rand_data()
hash = hashlib.md5(content).hexdigest()

if tentacles == True:
	filename = []
	content = []
	hash = []
	for i in range(10):
		filename.append(str(hex(random.randint(1,10000))[2:]))
		content.append(rand_data())
		hash.append(hashlib.md5(content[i]).hexdigest())

#Time between checks
pause_time=10

#Front towards enemy
armed_state=False

#Arm if first argument is "armed"
if sys.argv[1] == "armed":
	armed_state=True

def send_alert(msg):
	server = smtplib.SMTP('smtp.gmail.com:587')
	server.starttls()
	server.login(username,password)
	server.sendmail(fromaddr, toaddr, msg)
	server.quit()
	
#Removes the file on close
#This helps hide the presence of cryptolocked
#It also lessens the likihood of permission errors
def safe_close(signal, frame):
	if tentacles:
		for finame in filename:
			destroy_file(finame)
	destroy_file(filename)
	sys.exit(0)

#Creates the random data to fill a file with
def rand_data():
	dat = ''
	for i in range(100):
		dat = dat + str(random.randint(1,10000))
	return dat

#Create a file
def create_file(filename, content, hash):
	fi = open(filename,'w')
	fi.write(content)
	fi.close()
	if not file_integrity(filename, hash):
		print "ERROR CREATING FILE", filename
		sys.exit(1)
	
	
#Destroy a file
def destroy_file(filename):
	try:
		os.remove(filename)
	except:
		print "File was previously erased"
	
#return whether or not a file exists
def file_exists(filename):
	return os.path.isfile(filename)
	
#Check the integrity of a file
def file_integrity(filename, hash):
	fi = open(filename,'r')
	data = fi.read()
	_hash = hashlib.md5(data).hexdigest()
	return hash == _hash
	
#Determine the functionality of the script
#Checks for permission errors and what not
#All checks should return true
#To run these tests set the first argument to "debug"
def functionality_test():
	print "Checking if file exists:    ", ("File existed previously" if file_exists(filename) else True)
	if not file_exists(filename):
		create_file(filename, content, hash)
	print "Checking if file created:   ", ("File not created" if not file_exists(filename) else True)
	print "Checking if file written:   ", ("File not written properly" if not file_integrity(filename, hash) else True)
	destroy_file(filename)
	print "Checking if file destroyed: ", ("File not destroyed properly" if file_exists(filename) else True)
	print "If all \"True\" functionality is good"

#The main loop
#checks for file integrity violations and file destruction
def watch_loop():
	if tentacles:
		while True:
			for i in range(len(filename)):
				if not file_exists(filename[i]):
					print "FILE DESTRUCTION", filename[i]
					return True
				if not file_integrity(filename[i], hash[i]):
					print "INTEGRITY FAIL", filename[i]
					return True
		
			time.sleep(pause_time)
	else:
		while True:
			if not file_exists(filename):
				print "FILE DESTRUCTION"
				return True
			if not file_integrity(filename, hash):
				print "INTEGRITY FAIL"
				return True
			
			time.sleep(pause_time)
		
#The script's countermeasures
def countermeasures(armed=False):
	if tentacles:
		for finame in filename:
			destroy_file(finame)
	else:
		destroy_file(filename)
	#Simulated Action
	if not armed:
		print "Trigger Simulated Failsafe"
		return True

	#I really want a windows version of LSOF here.
	
	#Some possible countermeasures
	#	email alerts
	#	ps listing sent via email + shutdown
	#	system knockdown, won't turn on without recovery.. 
	#	posix LSOF monitoring to watch for file handle references to filename... attack offenders
	#		^My favorite
	#	shutdown + boot to safemode + warning banner
	#	shutdown + startups reset + warning banner
	#	Crypto-library hunt (takes time, should be done via livecd)
	#	Boot to secondary OS, perform network backup
	
	#If email alerts have been enabled send an alert
	if alerts_enabled:
		msg = "Cryptolocked Alert.  A failsafe has been triggered\n\n"
		msg = msg + "system IP: " + get_IP() + "\n"
		if sensitive_alerts:
			msg = msg + "Host info: " + get_hostname() + "\n"
			msg = msg + "Process Data: " + get_processes() + "\n"
		send_alert(msg)
	
	#Catch all simple failsafe
	if os.name == "nt":
		os.system("shutdown -s -t 0")
	if os.name == "posix":
		os.system("shutdown -h now")
		
def get_IP():
	if os.name == "nt":
		return os.popen('ipconfig').read()
	if os.name == "posix":
		return os.popen('ifconfig').read()

def get_hostname():
	if os.name == "nt":
		return os.popen('nbtstat -n').read()
	if os.name == "posix":
		return os.popen('hostname').read()
		
def get_processes():
	if os.name == "nt":
		return os.popen('tasklist').read()
	if os.name == "posix":
		return os.popen('ps aux').read()
		
#Initialization function
#Creates the file
def init(filename, content, hash):
	print "Checking if tripfile " + filename + " exists:    ", ("File existed previously" if file_exists(filename) else False)
	if file_exists(filename):
		print "tripfile Destroyed"
		destroy_file(filename)
	print "tripfile Instantiated"
	create_file(filename, content, hash)
		
#Main portion of the program
if __name__ == "__main__":
	signal.signal(signal.SIGINT, safe_close)
	if sys.argv[1] == "debug":
		functionality_test()
	else:
		if tentacles:
			for i in range(len(filename)):
				init(filename[i],content[i],hash[i])
		else:
			init(filename,content,hash)
		if watch_loop():
			print countermeasures(armed=armed_state)
	